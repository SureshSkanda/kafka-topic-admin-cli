#! /bin/bash

APP_NAME="kafka-topic-admin-cli"
# Define the kafka-streams input topics here.
INPUT_TOPICS=""
# Define any intermediate topics used in the streams topology.
INTERMEDIATE_TOPICS=""

docker exec -it kafka kafka-streams-application-reset \
    --application-id $APP_NAME \
    --input-topics $INPUT_TOPICS \
    --intermediate-topics $INTERMEDIATE_TOPICS \
    --bootstrap-servers localhost:29092 \
    --zookeeper zookeeper:22181

rm -rf /tmp/kafka-streams
