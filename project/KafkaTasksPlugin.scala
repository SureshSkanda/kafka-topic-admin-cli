import sbt._

import scala.sys.process._

object KafkaTasksPlugin extends AutoPlugin {

  object autoImport {
    lazy val startKafka   = taskKey[Unit]("Start kafka cluster.")
    lazy val stopKafka    = taskKey[Unit]("Stop kafka cluster.")
    lazy val restartKafka = taskKey[Unit]("Restart kafka cluster.")
    lazy val statusKafka  = taskKey[Unit]("Check kafka cluster status.")
  }

  import autoImport._

  override def globalSettings: Seq[Setting[_]] = Seq(
    startKafka := { "./docker/dev/init.sh start" ! },
    stopKafka := { "./docker/dev/init.sh stop" ! },
    restartKafka := { "./docker/dev/init.sh restart" ! },
    statusKafka := { "./docker/dev/init.sh status" ! }
  )

}
