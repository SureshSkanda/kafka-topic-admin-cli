[![pipeline status](https://gitlab.com/kpmeen/kafka-topic-admin-cli/badges/master/pipeline.svg)](https://gitlab.com/kpmeen/kafka-topic-admin-cli/commits/master)
[![coverage report](https://gitlab.com/kpmeen/kafka-topic-admin-cli/badges/master/coverage.svg)](https://gitlab.com/kpmeen/kafka-topic-admin-cli/commits/master)

# kafka-topic-admin-cli

The kafka topic admin is a CLI application that provides a way to manage
and administrator topics in Apache Kafka. Its mainly intended to be used in
conjunction with Ansible to ensure that all necessary topics are initialised in
Kafka before the producer/consumer applications that need them are deployed.


_Work In Progress_

## Configuration

| key                                                          | type          | value(s)                             | required |
|:-------------------------------------------------------------|:--------------|:-------------------------------------|:--------:|
| `net.scalytica.kafka.admin.clientId`                                | String        | An id used to identify the application against Kafka | y        |
| `net.scalytica.kafka.admin.kafkaUrl`                                | String        | A comma separated string with the host:port of all brokers in the kafka cluster | y        |
| `net.scalytica.kafka.admin.topics`                                  | Object List   | A list of topic configuration objects | y        |
| `net.scalytica.kafka.admin.topics.[i].name`                         | String        | Name to give the topic | y        |
| `net.scalytica.kafka.admin.topics.[i].partitions`                   | Integer > 1   | Number of partitions to give the topic | y        |
| `net.scalytica.kafka.admin.topics.[i].replicationFactor`            | Integer > 1   | Number of brokers to keep a replica of the topic | y        |
| `net.scalytica.kafka.admin.topics.[i].cleanupPolicy`                | String/Object | What kafka should do when / if cleanup is necessary. Values can be those for `type` or an Object with below attributes. | y        |
| `net.scalytica.kafka.admin.topics.[i].cleanupPolicy.type`           | String        | `compact` or `delete` | y        |
| `net.scalytica.kafka.admin.topics.[i].cleanupPolicy.deleteInterval` | Duration      | See HOCON docs | y        |
| `net.scalytica.kafka.admin.topics.[i].retention`                    | String/Object | One of `compact` or `delete`. Or config object with below attributes. | n        |
| `net.scalytica.kafka.admin.topics.[i].retention.size`               | Integer       | Size of topic in bytes. | n        |
| `net.scalytica.kafka.admin.topics.[i].retention.duration`           | Duration      | See HOCON docs | n        |

## Creating topics in batch

The application supports creating topics in "batch" mode. This can be achieved
in two ways.

1. **String argument**
   The String argument approach requires the String to be formatted in a
   particular way. Configuration for each topic should be enclosed in curly
   braces (`{` and `}`), each parameter separated with a `;` (semi-colon), an
   each block of braces must be separated with a comma. Ordering of config
   parameters does not matter.

   **example command**:

   ```bash
   create-topics --string "{name=foo;partitions=5;replication-factor=2;cleanup-policy=delete;retention-size=-1;retention-duration=-1},{name=bar;replication-factor=2},{name=baz;retention-duration=7days}"
   ```

2. **File argument**
   The second option is to provide a file containing the configuration for the
   topics to be created. The configuration syntax for the file is in the
   [HOCON](https://github.com/lightbend/config/blob/master/HOCON.md) format.
   Which is the same format as the `application.conf` for this application.
  
   **example config** (see `src/test/resources/create-topics-test.conf`):
  
   ```hocon
   net.scalytica.kafka.admin.topics = [
     {
       name = "topic1"
       partitions = 3
       replicationFactor = 1
       cleanupPolicy = "compact"
     },
     {
       name = "topic2"
       partitions = 1
       replicationFactor = 1
       cleanupPolicy = "delete"
       retention {
         duration = 7 days
       }
     },
     {
       name = "topic3"
       partitions = 3
       replicationFactor = 1
       cleanupPolicy = {
         type = "compact"
         deleteInterval = 48 hours
       }
       retention {
         size = 256
       }
     },
     {
       name = "topic4"
       partitions = 1
       replicationFactor = 1
       cleanupPolicy = "delete"
       retention {
         duration = 2 days
         size = 256
       }
     }
   ]
   ```
  
   **example command**:
  
   ```bash
   create-topics --file /path/to/some/file/topic-config.conf
   ```

## Usage with Ansible

The main driver behind this application was to have a way to initialize or
re-configure topics when deploying kafka services through Ansible. The most
sensible way to do this is by using the `—file` argument. This implies that a
topic configuration file must be present on the host that will be executing the
`create-topics` command.

The application is packaged into a zip archive, together with auto-generated
startup scripts, and bundled into a docker image. When deploying the application
to an environment it is done by starting a docker container with a mounted
volume containing the topic configurations.

**Important**

Understanding how topics work and how to configure them is very important, and
should not be left to chance. Please follow guidelines on this topic in the
[official documentation](https://kafka.apache.org) or from
[Confluent](http://confluent.io/docs/current).

### Example setup for Ansible role

Assuming the following folder structure for a role called `kafka-topics`:

```
roles/
  ...
  kafka-topics/
    defaults/
      main.yml
    files/
      topics.conf
    tasks/
      main.yml
  ...
```

The `topics.conf` file in this context is a HOCON formatted configuration file
as described above in the "File argument" chapter.

In the `defaults/main.yml` we define some variables for use in the tasks:

```yaml
kafka_topic_admin_cli:
  version: latest
  docker_image: <url_to_docker_registry>/kafka-topic-admin-cli
  container_name: kafka-topic-admin-cli
  environment_variables:
    # Required for running the kafka-topic-admin-cli app
    KAFKA_URL: <url to kafka brokers in cluster>
```

For the `tasks/main.yml`, the following tasks could be defined to create or update
topic configurations. We're going to assume that there is already a task for
installing docker on the target host:

```yaml

---
- name: 'KAFKA-TOPICS | Ensure topic-admin-config folder exists'
  file:
    path: /opt/topic-admin-config
    state: directory
    mode: 0755

- name: 'KAFKA-TOPICS | Copy topic-admin-config'
  copy:
    src: ../files/topics.conf
    dest: /opt/topic-admin-config/topics.conf
    mode: 0700

- name: 'KAFKA-TOPICS | Stopping docker container if it is already running'
  shell: 'docker stop {{ container_name }}'
  ignore_errors: true

- name: 'KAFKA-TOPICS | Removing docker container'
  shell: 'docker rm {{ container_name }}'
  ignore_errors: true

- name: 'KAFKA-TOPICS | Pull docker image'
  docker_image:
    name: '{{ kafka_topic_admin_cli.docker_image }}:{{ kafka_topic_admin_cli.version }}'
    pull: yes
    force: true

- name: 'KAFKA-TOPICS | Execute kafka-topic-admin-cli app'
  docker_container:
    name: '{{ container_name }}'
    image: '{{ kafka_topic_admin_cli.docker_image }}:{{ kafka_topic_admin_cli.version }}'
    command: create-update-topics --file /opt/docker/conf/topics.conf
    state: started
    restart_policy: 'no'
    volumes:
      - '/opt/topic-admin-config/topics.conf:/opt/docker/conf/topics.conf:ro'
    env: '{{ kafka_topic_admin_cli.environment_variables }}'
    ports: []

```

## Usage output

```bash
Kafka Topic Admin CLI
Usage: kafka-topic-admin-cli [topic-names|describe-topic|describe-log-dirs|create-topic|create-update-topics|update-topic|delete-topic] [options] <args>...



  --help                   prints this usage text


  -b, --kafka-brokers <host1:port>,<host2:port>,...
                           Comma separated list of URLs to the kafka brokers.


Command: topic-names [options]
List all non-internal topic names in the kafka cluster.
  -i, --include-internal   Include kafka internal topics


Command: describe-topic <topicName1>,<topicName2>,...
Show detailed topology information for a topic.
  <topicName1>,<topicName2>,...
                           A comma separated list of topic names.


Command: describe-log-dirs [options] <brokerId1>,<brokerId2>,...
Show detailed information about brokers
  <brokerId1>,<brokerId2>,...
                           A comma separated list of broker ID's
  -t, --topic <value>      Only show broker details where given topic is present.


Command: create-topic [options] <topicName>
Create a new topic in the kafka cluster
  <topicName>              Name of the topic to create
  -p, --partitions <value>
                           The total number of partitions to assign to the topic.
  -rf, --replication-factor <value>
                           On how many brokers should the topic be replicated.
  -c, --cleanup-policy <value>

  -rs, --retention-size <value>
                           Size limit of topic, in bytes, when records can be removed.
  -rd, --retention-duration <value>
                           How long records should be kept in the topic.


Command: create-update-topics [options]
Create or update a batch of topics by parsing a string of configs
  -s, --string <configString>
                           A properly formatted string containing the topic configs.
  -f, --file <fileName>    A file containing HOCON formatted topic configurations.


Command: update-topic [options] <topicName>
Update a topic by providing a new configuration, or increasing its number of partitions.
  <topicName>              Name of the topic to update
  -p, --partitions <value>
                           New total number of partitions to give the topic. NOTE: can only be incremented!
Command: delete-topic <topicName>
Delete a topic from the kafka cluster. ¡¡¡WARNING!!!
  <topicName>              Name of the topic to delete

```
