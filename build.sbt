import sbt.Keys._
import sbt._

// scalastyle:off

organization := "net.scalytica"
scalaVersion := "2.12.10"

name := "kafka-topic-admin-cli"

licenses += ("Apache-2.0", url(
  "http://opensource.org/licenses/https://opensource.org/licenses/Apache-2.0"
))

lazy val root =
  (project in file("."))
    .enablePlugins(DockerPlugin, JavaAppPackaging, KafkaTasksPlugin)

//----------------------------------------------------------------------------
// Compiler flags that enable certain language features, and adds checks to
// prevent "bad" code.
//----------------------------------------------------------------------------
scalacOptions := Seq(
  "-encoding",
  "utf-8", // Specify character encoding used by source files.
  "-feature", // Emit warning and location for usages of features that should be imported explicitly.
  "-deprecation", // Emit warning and location for usages of deprecated APIs.
  "-unchecked", // Enable additional warnings where generated code depends on assumptions.
  "-explaintypes", // Explain type errors in more detail.
  "-Xfuture", // Turn on future language features.
  "-Xcheckinit", // Wrap field accessors to throw an exception on uninitialized access.
  "-Xfatal-warnings", // Fail the compilation if there are any warnings.
  "-Xlint:adapted-args", // Warn if an argument list is modified to match the receiver.
  "-Xlint:by-name-right-associative", // By-name parameter of right associative operator.
  "-Xlint:constant", // Evaluation of a constant arithmetic expression results in an error.
  "-Xlint:delayedinit-select", // Selecting member of DelayedInit.
  "-Xlint:doc-detached", // A Scaladoc comment appears to be detached from its element.
  "-Xlint:inaccessible", // Warn about inaccessible types in method signatures.
  "-Xlint:infer-any", // Warn when a type argument is inferred to be `Any`.
  "-Xlint:missing-interpolator", // A string literal appears to be missing an interpolator id.
  "-Xlint:nullary-override", // Warn when non-nullary `def f()' overrides nullary `def f'.
  "-Xlint:nullary-unit", // Warn when nullary methods return Unit.
  "-Xlint:option-implicit", // Option.apply used implicit view.
  "-Xlint:package-object-classes", // Class or object defined in package object.
  "-Xlint:poly-implicit-overload", // Parameterized overloaded implicit methods are not visible as view bounds.
  "-Xlint:private-shadow", // A private field (or class parameter) shadows a superclass field.
  "-Xlint:stars-align", // Pattern sequence wildcard must align with sequence component.
  "-Xlint:type-parameter-shadow", // A local type parameter shadows a type already in scope.
  "-Xlint:unsound-match", // Pattern match may not be typesafe.
  "-language:implicitConversions",
  "-language:experimental.macros", // Allow macro definition (besides implementation and application)
  "-language:higherKinds",
  "-language:existentials",
  "-language:postfixOps",
  "-Yno-adapted-args", // Do not adapt an argument list (either by inserting () or creating a tuple) to match the receiver.
  "-Ypartial-unification", // Enable partial unification in type constructor inference
  "-Ywarn-dead-code", // Warn when dead code is identified.
  "-Ywarn-extra-implicit", // Warn when more than one implicit parameter section is defined.
  "-Ywarn-numeric-widen", // Warn when numerics are widened.
  "-Ywarn-unused:implicits", // Warn if an implicit parameter is unused.
  "-Ywarn-unused:imports", // Warn if an import selector is not referenced.
  "-Ywarn-unused:locals", // Warn if a local definition is unused.
  "-Ywarn-unused:params", // Warn if a value parameter is unused.
  "-Ywarn-unused:patvars", // Warn if a variable bound in a pattern is unused.
  "-Ywarn-unused:privates" // Warn if a private member is unused.
  // "-Ywarn-value-discard", // Warn when non-Unit expression results are unused.
)

//----------------------------------------------------------------------------
// Test configuration
//----------------------------------------------------------------------------
fork in Test := true               // Run tests in separate JVM.
parallelExecution in Test := false // Run tests sequentially
logBuffered in Test := false
testOptions in Test += Tests.Argument("-oD")
javaOptions in Test += "-Dlogger.resource=logback-test.xml"

//----------------------------------------------------------------------------
// Disable ScalaDoc generation
//----------------------------------------------------------------------------
publishArtifact in (Compile, packageDoc) := false
publishArtifact in packageDoc := false
sources in (Compile, doc) := Seq.empty

//----------------------------------------------------------------------------
// Docker settings
//----------------------------------------------------------------------------
val GitlabRegistry = "registry.gitlab.com"
val GitlabUser     = "kpmeen"

version in Docker := {
  if (version.value.contains("SNAPSHOT")) "latest" else version.value
}
maintainer in Docker := maintainer.value
dockerRepository := Some(s"$GitlabRegistry/$GitlabUser")
dockerAlias := {
  val tag = version.value
  DockerAlias(
    Some(GitlabRegistry),
    Some(GitlabUser),
    moduleName.value,
    Some(tag)
  )
}
dockerUpdateLatest := {
  val snapshot = isSnapshot.value
  val log      = sLog.value
  if (!snapshot) {
    log.info("Building release, updating docker latest tag")
    true
  } else {
    log.info("Building SNAPSHOT, not updating latest tag")
    false
  }
}

//----------------------------------------------------------------------------
// Resolvers & Dependencies
//----------------------------------------------------------------------------
resolvers ++= DefaultOptions.resolvers(snapshot = false) ++ Seq(
  Resolver.typesafeRepo("releases"),
  Resolver.jcenterRepo
)

publishMavenStyle := true
publishArtifact in Test := false
pomExtra := {
  <url>https://gitlab.com/kpmeen/kafka-topic-admin-cli</url>
    <scm>
      <url>git@gitlab.com:kpmeen/kafka-topic-admin-cli.git</url>
      <connection>scm:git:git@gitlab.com:kpmeen/kafka-topic-admin-cli.git</connection>
    </scm>
    <developers>
      <developer>
        <id>kpmeen</id>
        <name>Knut Petter Meen</name>
        <url>http://scalytica.net</url>
      </developer>
    </developers>
}

val slf4jVersion         = "1.7.30"
val logbackVersion       = "1.2.3"
val scalaTestVersion     = "3.1.2"
val embeddedKafkaVersion = "2.5.0"
val kafkaVersion         = "2.5.0"
val scalaLoggingVersion  = "3.9.2"
val configVersion        = "1.4.0"
val ficusVersion         = "1.4.7"
val scoptVeresion        = "3.7.1"

libraryDependencies ++= Seq(
  "org.slf4j"        % "slf4j-api"        % slf4jVersion,
  "org.slf4j"        % "log4j-over-slf4j" % slf4jVersion,
  "ch.qos.logback"   % "logback-classic"  % logbackVersion,
  "com.typesafe"     % "config"           % configVersion,
  "com.iheart"       %% "ficus"           % ficusVersion,
  "com.github.scopt" %% "scopt"           % scoptVeresion,
  "org.apache.kafka" % "kafka-clients"    % kafkaVersion exclude ("org.slf4j", "slf4j-log4j12"),
  "org.apache.kafka" %% "kafka"           % kafkaVersion excludeAll (
    ExclusionRule("org.slf4j", "slf4j-log4j12"),
    ExclusionRule("org.apache.zookeeper", "zookeeper")
  ),
  "com.typesafe.scala-logging" %% "scala-logging"  % scalaLoggingVersion,
  "org.scalatest"              %% "scalatest"      % scalaTestVersion % Test,
  "org.scalactic"              %% "scalactic"      % scalaTestVersion % Test,
  "io.github.embeddedkafka"    %% "embedded-kafka" % embeddedKafkaVersion % Test exclude ("log4j", "log4j")
)
