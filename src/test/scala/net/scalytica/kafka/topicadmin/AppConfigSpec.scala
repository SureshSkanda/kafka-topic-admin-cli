package net.scalytica.kafka.topicadmin

import com.typesafe.config.ConfigFactory
import net.scalytica.kafka.topicadmin.models._
import org.scalatest.Inspectors.forAll
import org.scalatest.OptionValues
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.concurrent.duration._

// scalastyle:off magic.number
class AppConfigSpec extends AnyWordSpec with Matchers with OptionValues {

  val testCfg = ConfigFactory.parseResources("application-test.conf").resolve()

  "The AppConfig" should {

    "parse the default config file" in {
      val ac = AppConfig.load()

      ac.kafkaUrl mustBe "localhost:29092"
      ac.clientId mustBe "kafka-topic-admin-cli"
      ac.topics mustBe empty
    }

    "parse the test config file" in {
      val ac = AppConfig.fromConfig(testCfg)

      ac.kafkaUrl mustBe "localhost:29092"
      ac.clientId mustBe "kafka-topic-admin-cli"
      ac.topics must have size 4

      val sortedTopics = ac.topics.sortBy(_.name)

      forAll(sortedTopics.zipWithIndex) {
        case (t, idx) =>
          t.name mustBe s"topic${idx + 1}"
          t.replicationFactor mustBe 1
      }

      val first  = ac.topics.head
      val second = ac.topics(1)
      val third  = ac.topics(2)
      val fourth = ac.topics(3)

      first.partitions mustBe 3
      second.partitions mustBe 1
      third.partitions mustBe 3
      fourth.partitions mustBe 1

      first.cleanupPolicy mustBe Compact()
      second.cleanupPolicy mustBe Delete
      third.cleanupPolicy mustBe Compact(Some(48 hours))
      fourth.cleanupPolicy mustBe Delete

      first.retention.value mustBe InfiniteRetention
      second.retention.value mustBe TimeRetention(7 days)
      third.retention.value mustBe SizeRetention(256)
      fourth.retention.value mustBe TimeAndSizeRetention(2 days, 256)
    }

  }

}
