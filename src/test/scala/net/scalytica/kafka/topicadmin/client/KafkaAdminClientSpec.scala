package net.scalytica.kafka.topicadmin.client

import com.typesafe.config.ConfigFactory
import net.manub.embeddedkafka.ConsumerExtensions.ConsumerRetryConfig
import net.manub.embeddedkafka.{EmbeddedKafka, EmbeddedKafkaConfig}
import net.scalytica.kafka.topicadmin.AppConfig
import net.scalytica.kafka.topicadmin.models._
import org.apache.kafka.common.config.TopicConfig._
import org.apache.kafka.common.serialization.StringSerializer
import org.scalatest.Inspectors.forAll
import org.scalatest._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Seconds, Span}
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

// scalastyle:off magic.number
class KafkaAdminClientSpec
    extends AnyWordSpec
    with Matchers
    with ScalaFutures
    with OptionValues
    with EitherValues
    with EmbeddedKafka
    with BeforeAndAfterAll {

  override implicit val patienceConfig: PatienceConfig =
    PatienceConfig(timeout = Span(20, Seconds))

  implicit val consumerRetryConfig: ConsumerRetryConfig =
    ConsumerRetryConfig(poll = 100 millis, maximumAttempts = 20)

  implicit val ekc: EmbeddedKafkaConfig = EmbeddedKafkaConfig(23456, 23457)

  override def beforeAll(): Unit = {
    super.beforeAll()
    EmbeddedKafka.start()
  }

  override def afterAll(): Unit = {
    EmbeddedKafka.stop()
    super.afterAll()
  }

  val url = s"localhost:${ekc.kafkaPort}"
  val cid = "topic-admin-test-client"

  val testCfg = ConfigFactory.parseResources("application-test.conf").resolve()

  val cfg = AppConfig.fromConfig(testCfg).copy(kafkaUrl = url, clientId = cid)

  val client = KafkaAdminClient(cfg)

  "The admin client" should {

    "be correctly configured" in {
      client.kafkaUrl mustBe url
      client.clientId mustBe cid
    }

    "successfully create configured topics" in {
      client.createTopics(cfg.topics: _*).futureValue mustBe true
    }

    "successfully list all topic names" in {
      val names = client.listAllTopicNames().futureValue
      names must not be empty

    }

    "successfully list all non-internal topics names" in {
      val names = client.listTopicNames().futureValue
      names must not be empty
    }

    "return true if a topic exists" in {
      val topicName = cfg.topics.headOption.value.name
      client.exists(topicName).futureValue mustBe true
    }

    "return true if all provided topics exist" in {
      client.exists(cfg.topics.map(_.name): _*).futureValue mustBe true
    }

    "return false if 1 topic of many doesn't exist" in {
      val topics = cfg.topics.map(_.name) :+ "non_existing_topic"
      client.exists(topics: _*).futureValue mustBe false
    }

    "successfully describe a given topic" in {
      val topic = cfg.topics.headOption.value

      val res = client.describeTopic(topic.name).futureValue

      res must not be empty

      res.value.internal mustBe false
      res.value.name mustBe "topic1"
      res.value.partitions must have size 3
      forAll(res.value.partitions) { partition =>
        partition.leader.id mustBe 0
        partition.leader.host mustBe "localhost"
        partition.leader.port mustBe ekc.kafkaPort
        partition.leader.rack mustBe None
      }
    }

    "successfully list configurations for several topics" in {
      val topics = cfg.topics.take(1)

      val res = client.describeTopicConfig(topics.map(_.name): _*).futureValue

      res must have size 1

      val td = res.headOption.value
      td.name mustBe topics.head.name
      td.config.keySet must contain allElementsOf Seq(
        DELETE_RETENTION_MS_CONFIG,
        CLEANUP_POLICY_CONFIG,
        RETENTION_BYTES_CONFIG,
        RETENTION_MS_CONFIG
      )
    }

    "successfully list log dir info for kafka brokers" in {
      val res = client.describeLogDirectories(0).futureValue

      res must have size 1
      res.headOption.value.id mustBe 0
      res.headOption.value.directories must have size 1
      res.headOption.value.directories.headOption.value.info must have size 8
    }

    "successfully show log dir info for kafka brokers with a given topic" in {
      implicit val strSer: StringSerializer = new StringSerializer()

      val topic = cfg.topics.headOption.value

      // place some messages in the first topic to ensure fields are set in
      // the describeLogDirectories response.
      (0 to 1000).foreach { i =>
        publishToKafka(topic.name, s"$i", s"message$i")
      }

      val res =
        client.describeLogDirectoriesForTopic(Seq(0), topic.name).futureValue

      res must have size 1
      val topic1 = res.headOption.value

      topic1.id mustBe 0
      topic1.directories must have size 1

      val dir1 = topic1.directories.headOption.value
      dir1.directory must not be empty
      dir1.info must have size 3
      forAll(dir1.info.toSeq) {
        case (topicPartition, replicaInfo) =>
          topicPartition.topic mustBe topic.name
          replicaInfo.size mustBe >(20000L)
          replicaInfo.isFuture mustBe false
          replicaInfo.offsetLag mustBe 0
      }
    }

    "successfully modify a topic configuration" in {
      val org = cfg.topics.headOption.value
      val topic = org.copy(
        cleanupPolicy = Delete,
        retention = Some(TimeRetention(5 seconds))
      )

      val res = client.alterTopicConfig(topic).futureValue

      res must not be empty
    }

    "successfully increase number of partitions on a given topic" in {
      val t = Topic("clean-me-up", 1)
      client.createTopics(t).futureValue mustBe true
      client.increaseNumTopicPartitions(t.name, 3).futureValue mustBe true
      client.describeTopic(t.name).futureValue.value.partitions.size mustBe 3
      client.deleteTopic(t.name).futureValue mustBe true
    }

    "successfully delete a topic" in {
      val topic = Topic("delete_me_topic", 1)
      client.createTopics(topic).futureValue mustBe true
      client.deleteTopic(topic.name).futureValue mustBe true
      client.exists(topic.name).futureValue mustBe false
    }

    "successfully delete several topics" in {
      val topics = cfg.topics.map(_.name)
      val res    = client.deleteTopics(topics).futureValue

      forAll(res.toList) {
        case (topicName, deleted) =>
          deleted mustBe true
          client.exists(topicName).futureValue mustBe false
      }
    }

  }

}
