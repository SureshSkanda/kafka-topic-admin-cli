package net.scalytica.kafka.topicadmin.client

import java.time.{Duration => JDuration}

import com.typesafe.scalalogging.LazyLogging
import net.scalytica.kafka.topicadmin.KafkaFutureImplicits._
import net.scalytica.kafka.topicadmin.models.{
  BrokerLogDirInfo,
  Topic,
  TopicConfigDetails,
  TopicTopologyDescription
}
import net.scalytica.kafka.topicadmin.{AppConfig, BrokerId, TopicName, _}
import org.apache.kafka.clients.admin.AdminClientConfig._
import org.apache.kafka.clients.admin._
import org.apache.kafka.common.KafkaFuture
import org.apache.kafka.common.config.ConfigResource
import org.apache.kafka.common.config.SaslConfigs.{
  SASL_JAAS_CONFIG,
  SASL_MECHANISM
}
import org.apache.kafka.common.config.SslConfigs._

import scala.collection.JavaConverters._
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.util.control.NonFatal
import scala.util.{Failure, Success, Try}

/**
 * Provides a wrapper around the default Kafka admin client. Its API is tailored
 * specifically for the purpose of this project.
 *
 * @param clientId the client id to identify as when communicating with Kafka
 * @param kafkaUrl the URLs to the Kafka cluster as a comma separated String.
 */
case class KafkaAdminClient(
    clientId: String = "",
    kafkaUrl: String = KafkaAdminClient.DefaultKafkaUrl,
    clientProps: Map[String, AnyRef] = Map.empty
) extends LazyLogging {

  // scalastyle:off line.size.limit
  private[this] lazy val admConfig = Map[String, AnyRef](
    BOOTSTRAP_SERVERS_CONFIG       -> kafkaUrl,
    CLIENT_ID_CONFIG               -> clientId,
    SEND_BUFFER_CONFIG             -> KafkaAdminClient.DefaultSendBufferBytes.toString,
    RECEIVE_BUFFER_CONFIG          -> KafkaAdminClient.DefaultReceiveBufferBytes.toString,
    METADATA_MAX_AGE_CONFIG        -> KafkaAdminClient.DefaultMetadataMaxAge.toString,
    RECONNECT_BACKOFF_MS_CONFIG    -> KafkaAdminClient.DefaultReconnectBackoffMs.toString,
    RETRY_BACKOFF_MS_CONFIG        -> KafkaAdminClient.DefaultRetryBackoffMs.toString,
    REQUEST_TIMEOUT_MS_CONFIG      -> KafkaAdminClient.DefaultRequestTimeoutMs.toString,
    CONNECTIONS_MAX_IDLE_MS_CONFIG -> KafkaAdminClient.DefaultConnectionsMaxIdleMs.toString
  ) ++ clientProps
  // scalastyle:on line.size.limit

  private[this] lazy val underlying = AdminClient.create(admConfig.asJava)

  /**
   * Lists all topics in the kafka cluster. This includes internal topics that
   * are used by the kafka cluster itself. To only list the non-internal topics,
   * see [[listTopicNames()]] below.
   *
   * @return Eventually returns a Set of topic names.
   */
  def listAllTopicNames(): Future[Set[String]] =
    underlying.listTopics().names().call(n => Set[String](n.asScala.toSeq: _*))

  /**
   * Lists all non-internal topics in the kafka cluster. To also list the
   * internal topics, please see [[listAllTopicNames()]]
   *
   * @return Eventually returns a Set of topic names.
   */
  def listTopicNames(): Future[Set[String]] = {
    val listOpts = new ListTopicsOptions().listInternal(false)
    underlying
      .listTopics(listOpts)
      .names()
      .call(n => Set[String](n.asScala.toSeq: _*))
  }

  /**
   * Provides a [[TopicTopologyDescription]] for the given topic. Containing
   * information about the partitions and replicas containing the topic data.
   *
   * @param topicName the name of the topic to get a description for
   * @return eventually returns an {{{Option[TopicTopologyDescription]}}}
   */
  def describeTopic(
      topicName: TopicName
  ): Future[Option[TopicTopologyDescription]] =
    underlying
      .describeTopics(Seq(topicName).asJava)
      .all()
      .call(_.asScala.headOption.map(_._2))

  /**
   * Provides a [[TopicTopologyDescription]] for the given topics. Containing
   * information about the partitions and replicas containing the topic data.
   *
   * @param topicName the name of the topic to get a description for
   * @return eventually returns a {{{Map[String, TopicTopologyDescription]}}}
   */
  def describeTopics(
      topicName: TopicName*
  ): Future[Map[String, TopicTopologyDescription]] =
    underlying
      .describeTopics(topicName.asJava)
      .all()
      .call(
        _.asScala.toMap
          .map(kv => kv._1 -> TopicTopologyDescription.fromKafkaModel(kv._2))
      )

  /**
   * List topic configuration details for the given topics
   *
   * @param topicName the topic names to get config details for
   * @return eventually returns a list of [[TopicConfigDetails]]
   */
  def describeTopicConfig(
      topicName: TopicName*
  ): Future[Seq[TopicConfigDetails]] = {
    val cfgResources = topicName
      .map(n => new ConfigResource(ConfigResource.Type.TOPIC, n))
      .asJava

    underlying.describeConfigs(cfgResources).all().call { r =>
      r.asScala.toSeq.map { crc =>
        val name = crc._1.name()
        val cfg  = crc._2.entries().asScala.map(ce => ce.name() -> ce.value())
        TopicConfigDetails(name, cfg.toMap)
      }
    }
  }

  /**
   * Provides a textual description of the topic partitions and replicas.
   *
   * @param topicName the name of the topic to get a description for
   * @param ec        the ExecutionContext to execute futures on.
   * @return eventually returns an {{{Option[String]}}} with the String repr.
   */
  def describeTopicAsString(
      topicName: TopicName
  )(implicit ec: ExecutionContext): Future[Option[String]] =
    describeTopic(topicName).map(_.map(_.toString))

  /**
   * Provide a list with information about the given brokers' log directories.
   *
   * @param brokers the ID of the brokers to show log directory info for
   * @return eventually returns a collection of [[BrokerLogDirInfo]] data
   */
  def describeLogDirectories(
      brokers: BrokerId*
  ): Future[Seq[BrokerLogDirInfo]] = {
    underlying
      .describeLogDirs(brokers)
      .all()
      .call(_.asScala.map(d => d: BrokerLogDirInfo).toSeq)
  }

  /**
   * Same functionality as for [[describeLogDirectories()]], except here the
   * extra {{{topicName}}} argument will filter the data structure so that it
   * only contains information about the logs relevant for that topic.
   *
   * @param brokers   the ID of the brokers to show log directory info for
   * @param topicName the name of the topic to include
   * @param ec        the ExecutionContext to execute futures on.
   * @return eventually returns a collection of [[BrokerLogDirInfo]] data
   */
  def describeLogDirectoriesForTopic(
      brokers: Seq[BrokerId],
      topicName: TopicName
  )(implicit ec: ExecutionContext): Future[Seq[BrokerLogDirInfo]] = {
    describeLogDirectories(brokers: _*).map { brokerLogDirs =>
      brokerLogDirs.map { b =>
        b.copy(
          directories = b.directories.map { dir =>
            dir.copy(info = dir.info.filter(_._1.topic == topicName))
          }
        )
      }
    }
  }

  /**
   * Will check to see if any of the provided topics already exist or not. If a
   * topic isn't found, the method will attempt to create it.
   *
   * @param topic The topics to create.
   * @param ec    the ExecutionContext to execute futures on.
   * @return Eventually returns true if all topics are available, else false.
   */
  def createTopics(
      topic: Topic*
  )(implicit ec: ExecutionContext): Future[Boolean] = {
    logger.info(
      s"Initializing the following topics if they don't already exist:" +
        topic.mkString("\n - ", "\n - ", "")
    )

    for {
      existing <- listAllTopicNames()
      toCreate = topic.filterNot(t => existing.contains(t.name))
      _            <- createKafkaTopics(toCreate)
      hasAllTopics <- exists(topic.map(_.name): _*)
    } yield hasAllTopics
  }

  /**
   * Will check to see if any of the provided topics already exist or not. If a
   * topic isn't found, the method will attempt to create it. Otherwise it will
   * try to update the topic configuration.
   *
   * @param topic The topics to create or update.
   * @param ec    the ExecutionContext to execute futures on.
   * @return Eventually returns true if all topics are available, else false.
   */
  def createOrUpdateTopics(
      topic: Topic*
  )(implicit ec: ExecutionContext): Future[Boolean] = {
    logger.info(
      "Initializing or updating the following " +
        s"topics:${topic.mkString("\n - ", "\n - ", "")}"
    )
    for {
      existing <- listAllTopicNames()
      toAdd = topic.filterNot(t => existing.contains(t.name))
      toUpd = topic.filter(t => existing.contains(t.name))
      _    <- createKafkaTopics(toAdd)
      _    <- alterKafkaTopics(toUpd)
      desc <- describeTopics(toUpd.map(_.name): _*)
      incParts = toUpd.filter(
        t =>
          desc.exists(
            td => t.name == td._1 && t.partitions > td._2.partitions.size
          )
      )
      _      <- increaseKafkaNumTopicPartitions(incParts)
      hasAll <- exists(topic.map(_.name): _*)
      // TODO: Check diff between old and new configs
    } yield hasAll
  }

  /**
   * Checks to see if kafka contains the provided topics.
   *
   * @param topicName Topic name(s) to check for.
   * @param ec        the ExecutionContext to execute futures on.
   * @return true if ALL the topic names exist. Otherwise false.
   */
  def exists(
      topicName: TopicName*
  )(implicit ec: ExecutionContext): Future[Boolean] =
    listTopicNames().map(found => topicName.forall(n => found.contains(n)))

  /**
   * Allows for modifying the topic configuration for a given [[Topic]].
   *
   * If the configuration change involves increasing the number of partitions
   * for the topic, please use [[increaseNumTopicPartitions()]] instead.
   *
   * @param topic the Topic to alter configuration for
   * @param ec    the ExecutionContext to execute futures on.
   * @return eventually returns an {{{Option}}} with the topic name. If the
   *         operation failed, the {{{Option}}} will be {{{None}}}.
   */
  def alterTopicConfig(
      topic: Topic
  )(implicit ec: ExecutionContext): Future[Option[TopicName]] = {
    val cr = new ConfigResource(ConfigResource.Type.TOPIC, topic.name)
    // TODO: Verify that the below structure has the same effect as the
    //       previous functionality using the deprecated "alterConfigs".
    val cfg = Map(
      cr -> topic.configEntries
        .map(ce => new AlterConfigOp(ce, AlterConfigOp.OpType.SET))
        .asJavaCollection
    )

    underlying
      .incrementalAlterConfigs(cfg.asJava)
      .values()
      .asScala
      .headOption
      .map {
        case (cfgRes, kf) =>
          kf.callVoid()
            .map { _ =>
              logger.info(s"Config change for topic '${topic.name}' applied.")
              Some(cfgRes.name())
            }
            .recover {
              case t: Throwable =>
                val msg = s"Updating config for topic '${topic.name}' failed"
                logger.warn(msg, t)
                None
            }
      }
      .getOrElse(Future.successful(None))
  }

  /**
   * Method for increasing the number of partitions allocated for a given topic.
   * The topic will get a <i>new total number of partitions</i> matching the
   * value of {{{newTotal}}}. However, it is <i>only possible to increase</i>
   * the number of partitions. Keep that in mind when using this method. Setting
   * a very high partition number may well have a negative impact of how the
   * topic performs. Be generous, but conservative!
   *
   * @param topicName the name of the topic to increase num partitions for
   * @param newTotal  the new total number of partitions for the topic. Must be
   *                  greater than the previous total number of partitions.
   * @param ec        the ExecutionContext to execute futures on.
   * @return eventually returns with a Unit result
   */
  def increaseNumTopicPartitions(
      topicName: TopicName,
      newTotal: Int
  )(implicit ec: ExecutionContext): Future[Boolean] =
    underlying
      .createPartitions(
        Map(topicName -> NewPartitions.increaseTo(newTotal)).asJava
      )
      .all()
      .callVoid()
      .map(_ => true)
      .recover {
        case NonFatal(ex) =>
          logger.warn(
            s"An error occurred when increasing num partitions for $topicName",
            ex
          )
          false
      }

  /**
   * Delete a single topic from the kafka cluster
   *
   * @param topicName name of the topic to delete
   * @param ec        the ExecutionContext to execute futures on.
   * @return eventually returns true if deletion succeeded, otherwise false
   */
  def deleteTopic(
      topicName: TopicName
  )(implicit ec: ExecutionContext): Future[Boolean] =
    evalTopicDeletion(
      topicName -> underlying.deleteTopics(Seq(topicName).asJava).all()
    ).map(_._2)

  /**
   * Delete all the provided topics from the kafka cluster
   *
   * @param topicNames the names of the topics to delete
   * @param ec         the ExecutionContext to execute futures on.
   * @return eventually returns a map containing the topic name and status of
   *         the deletion operation.
   */
  def deleteTopics(
      topicNames: Seq[TopicName]
  )(implicit ec: ExecutionContext): Future[Map[TopicName, Boolean]] =
    Future.sequence {
      underlying
        .deleteTopics(topicNames.asJava)
        .values()
        .asScala
        .map(t => evalTopicDeletion(t))
    }.map(_.toMap)

  /**
   * Close the underlying kafka admin client.
   */
  def close(): Unit = underlying.close()

  /**
   * Close the underlying kafka admin client, waiting for the provided timeout
   * duration for the close operation to complete.
   *
   * @param timeout time to wait for close operation to complete.
   */
  def close(timeout: FiniteDuration): Unit =
    underlying.close(JDuration.ofMillis(timeout.toMillis))

  // ===========================================================================
  // INTERNAL APIS
  // ===========================================================================

  private[this] def createKafkaTopics(topics: Seq[Topic]): Future[Unit] =
    underlying
      .createTopics(topics.map(_.newTopic).asJavaCollection)
      .all()
      .callVoid()

  private[this] def alterKafkaTopics(
      topics: Seq[Topic]
  )(implicit ec: ExecutionContext): Future[Try[Unit]] = {
    Future
      .sequence(topics.map(topic => alterTopicConfig(topic)))
      .map { res =>
        val altered   = res.collect { case Some(name) => name }
        val unchanged = topics.filterNot(t => altered.contains(t.name))

        if (altered.nonEmpty)
          logger.info(s"Altered topic configs for ${altered.mkString(", ")}")

        if (unchanged.nonEmpty)
          logger.warn(s"Did not alter configs for ${unchanged.mkString(", ")}")
      }
      .map(_ => Success(()))
      .recover {
        case NonFatal(ex) =>
          logger.warn(
            "An error occurred when trying to alter configs " +
              s"for ${topics.map(_.name).mkString(", ")}",
            ex
          )
          Failure(ex)
      }
  }

  private[this] def increaseKafkaNumTopicPartitions(
      topics: Seq[Topic]
  )(implicit ec: ExecutionContext): Future[Try[Unit]] = {
    val tpc = topics.map { t =>
      t.name -> NewPartitions.increaseTo(t.partitions)
    }.toMap
    underlying
      .createPartitions(tpc.asJava)
      .all()
      .callVoid()
      .map(_ => Success(()))
      .recover {
        case NonFatal(ex) =>
          logger.warn(
            "An error occurred when trying to increase partition count " +
              s"for ${tpc.keys.mkString(", ")}",
            ex
          )
          Failure(ex)
      }
  }

  private[this] def evalTopicDeletion(
      tuple: (TopicName, KafkaFuture[Void])
  )(implicit ec: ExecutionContext): Future[(TopicName, Boolean)] =
    tuple._2.callVoid().map(_ => tuple._1 -> true).recover {
      case NonFatal(_) => tuple._1 -> false
    }
}

object KafkaAdminClient {
  // A set of default configuration values for the admin client
  val DefaultKafkaUrl             = "localhost:9092"
  val DefaultMetadataMaxAge       = (5 * 60 * 1000).toLong
  val DefaultSendBufferBytes      = 5 * 60 * 1000
  val DefaultReceiveBufferBytes   = 64 * 1024
  val DefaultReconnectBackoffMs   = 50L
  val DefaultRetryBackoffMs       = 1000L
  val DefaultRequestTimeoutMs     = 120000L
  val DefaultConnectionsMaxIdleMs = (5 minutes).toMillis

  def apply(cfg: AppConfig): KafkaAdminClient = new KafkaAdminClient(
    clientId = cfg.clientId,
    kafkaUrl = cfg.kafkaUrl,
    clientProps = cfg.clientProperties
  )
  def apply(
      cfg: AppConfig,
      credentials: Option[(String, String)]
  ): KafkaAdminClient = {
    val authProps = credentials.map {
      case (u, p) =>
        val jaasCfg =
          "org.apache.kafka.common.security.plain.PlainLoginModule " +
            s"""required username="$u" password="$p";""".stripMargin
        Map(
          SSL_ENDPOINT_IDENTIFICATION_ALGORITHM_CONFIG -> "https",
          SASL_MECHANISM                               -> "PLAIN",
          SASL_JAAS_CONFIG                             -> jaasCfg,
          SECURITY_PROTOCOL_CONFIG                     -> "SASL_SSL"
        )
    }.getOrElse(Map.empty)

    new KafkaAdminClient(
      clientId = cfg.clientId,
      kafkaUrl = cfg.kafkaUrl,
      clientProps = cfg.clientProperties ++ authProps
    )
  }

}
