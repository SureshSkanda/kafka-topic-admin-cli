package net.scalytica.kafka.topicadmin

import com.typesafe.config.{Config, ConfigException}
import net.ceedubs.ficus.Ficus._
import net.ceedubs.ficus.readers.ValueReader
import net.scalytica.kafka.topicadmin.models._

import scala.concurrent.duration.FiniteDuration
import scala.util.{Success, Try}

trait ExtraConfigReaders {

  private[this] val expectedSimple = (s: String) =>
    s"Expected one of 'delete' or 'compact', but got '$s'."

  private[this] val expectedCompact = (s: String) =>
    s"Got $s. But Expected a config object with type 'compact' and an " +
      "optional deleteInterval with value type of 'FiniteDuration'"

  private[this] def illegalValue(cfg: Config, path: String, expMsg: String) =
    throw new ConfigException.BadValue(
      cfg.origin(),
      path,
      s"Unexpected configuration value for cleanupPolicy. $expMsg"
    )

  implicit val cleanupCfgReader: ValueReader[CleanupPolicy] =
    (cfg: Config, path: String) => {
      Try(cfg.getAs[String](path)) match {
        case Success(Some(policy)) =>
          if (policy == "delete") Delete
          else if (policy == "compact") Compact()
          else illegalValue(cfg, path, expectedSimple(policy))

        case _ =>
          cfg.getAs[Config](path) match {
            case Some(config) =>
              config.as[String]("type") match {
                case t: String if t == "compact" =>
                  Compact(config.getAs[FiniteDuration]("deleteInterval"))

                case t =>
                  illegalValue(config, s"$path.type", expectedCompact(t))
              }

            case None =>
              illegalValue(cfg, path, expectedSimple("an empty value"))
          }
      }
    }

  implicit val retentionCfgReader: ValueReader[Retention] =
    ValueReader.relative { cfg =>
      val maybeDuration = cfg.getAs[FiniteDuration]("duration")
      val maybeSize     = cfg.getAs[Int]("size")

      (maybeDuration, maybeSize) match {
        case (Some(dur), Some(size)) => TimeAndSizeRetention(dur, size)
        case (Some(dur), None)       => TimeRetention(dur)
        case (None, Some(size))      => SizeRetention(size)
        case (None, None)            => InfiniteRetention
      }
    }

}
