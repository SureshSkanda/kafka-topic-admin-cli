package net.scalytica.kafka.topicadmin.models

import net.scalytica.kafka.topicadmin.TopicName
import org.apache.kafka.clients.admin.TopicDescription
import org.apache.kafka.common.{
  TopicPartitionInfo,
  TopicPartition => KTopicPartition
}

import scala.collection.JavaConverters._

case class TopicConfigDetails(name: TopicName, config: Map[String, String]) {

  override def toString: String = {
    s"""Topic configuration details for $name:
       |${config.map(ce => s"""  ${ce._1}:  ${ce._2}""").mkString("\n")}
     """.stripMargin
  }

}

case class TopicPartition(topic: TopicName, partition: Int) {

  override def toString: String = {
    s"""TopicPartition{name: $topic, partition: $partition}"""
  }

}

object TopicPartition {

  implicit def fromKafkaModel(tp: KTopicPartition): TopicPartition = {
    TopicPartition(tp.topic(), tp.partition())
  }

}

case class TopicTopologyDescription(
    name: String,
    internal: Boolean = false,
    partitions: Seq[TopicPartitionDetails] = Seq.empty
) {

  override def toString = {
    s"""Topic topology description for $name
       | internal: $internal
       | partitions:\n${partitions.mkString("\n")}
     """.stripMargin
  }

}

object TopicTopologyDescription {

  implicit def fromKafkaModel(
      td: TopicDescription
  ): TopicTopologyDescription = {
    TopicTopologyDescription(
      name = td.name(),
      internal = td.isInternal,
      partitions = td.partitions().asScala
    )
  }

}

case class TopicPartitionDetails(
    partition: Int,
    leader: BrokerNode,
    replicas: Seq[BrokerNode] = Seq.empty,
    inSyncReplicas: Seq[BrokerNode] = Seq.empty
) {

  override def toString: String = {
    s"""  - Details for partition #$partition:
       |      leader node     : $leader
       |      replicas        :
       |        - ${replicas.mkString("\n        - ")}
       |      in sync replicas:
       |        - ${inSyncReplicas.mkString("        - ")}
     """.stripMargin
  }

}

object TopicPartitionDetails {

  implicit def fromKafkaModel(t: TopicPartitionInfo): TopicPartitionDetails = {
    TopicPartitionDetails(
      partition = t.partition(),
      leader = t.leader(),
      replicas = t.replicas().asScala,
      inSyncReplicas = t.isr().asScala
    )
  }

  implicit def fromSeqKafkaModel(
      s: Seq[TopicPartitionInfo]
  ): Seq[TopicPartitionDetails] = s.map(fromKafkaModel)

}
