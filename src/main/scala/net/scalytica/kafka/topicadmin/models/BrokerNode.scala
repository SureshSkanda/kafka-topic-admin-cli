package net.scalytica.kafka.topicadmin.models

import net.scalytica.kafka.topicadmin.BrokerId
import org.apache.kafka.common.Node

case class BrokerNode(
    id: BrokerId,
    host: String,
    port: Int,
    rack: Option[String]
) {

  override def toString: String = {
    s"BrokerNode: {" +
      s"id: $id, " +
      s"host: $host, " +
      s"port: $port, " +
      s"rack: ${rack.getOrElse("-")}" +
      s"}"
  }

}

object BrokerNode {

  implicit def fromKafkaNode(n: Node): BrokerNode = {
    BrokerNode(
      id = n.id(),
      host = n.host(),
      port = n.port(),
      rack = Option(n.rack())
    )
  }

  implicit def fromKafkaNodes(s: Seq[Node]): Seq[BrokerNode] =
    s.map(fromKafkaNode)

}
