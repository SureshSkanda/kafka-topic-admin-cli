package net.scalytica.kafka.topicadmin.models

import org.apache.kafka.common.config.TopicConfig.{
  CLEANUP_POLICY_COMPACT,
  CLEANUP_POLICY_CONFIG,
  CLEANUP_POLICY_DELETE,
  DELETE_RETENTION_MS_CONFIG
}

import scala.concurrent.duration._

sealed trait CleanupPolicy extends KafkaPropType

object CleanupPolicy {

  def fromString(s: String): CleanupPolicy = {
    if (s.toLowerCase == "delete") Delete
    else if (s.toLowerCase == "compact") Compact()
    else
      throw new IllegalArgumentException(
        s"'$s' is not a valid cleanup policy. Must be either delete or compact."
      )
  }

}

case object Delete extends CleanupPolicy {

  override def toPropMap = Map(CLEANUP_POLICY_CONFIG -> CLEANUP_POLICY_DELETE)

}

case class Compact(deleteInterval: Option[FiniteDuration] = None)
    extends CleanupPolicy {

  private[this] lazy val deleteRetentionMs = deleteInterval
    .getOrElse(Compact.DefaultRetentionDeleteDuration)
    .toMillis
    .toString

  override def toPropMap = Map(
    CLEANUP_POLICY_CONFIG      -> CLEANUP_POLICY_COMPACT,
    DELETE_RETENTION_MS_CONFIG -> deleteRetentionMs
  )

}

object Compact {
  val DefaultRetentionDeleteDuration = 24 hours
}
