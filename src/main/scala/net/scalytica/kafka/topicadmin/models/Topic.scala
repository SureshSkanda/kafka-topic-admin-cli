package net.scalytica.kafka.topicadmin.models

import com.typesafe.config.ConfigFactory
import net.ceedubs.ficus.Ficus._
import net.ceedubs.ficus.readers.ArbitraryTypeReader._
import net.scalytica.kafka.topicadmin.ExtraConfigReaders
import net.scalytica.kafka.topicadmin.TopicName
import org.apache.kafka.clients.admin.{ConfigEntry, NewTopic}

import scala.collection.JavaConverters._

/**
 * Class describing relevant configuration options for setting up a kafka topic.
 *
 * @param name              The name of the kafka topic.
 * @param partitions        The number of partitions to use for the topic.
 * @param replicationFactor How many replicas the topic should have.
 * @param cleanupPolicy     If messages should be deleted or compacted when
 *                          cleaning up.
 * @param retention         Which rules apply for retaining messages in a topic
 * @see [[CleanupPolicy]]
 * @see [[Retention]]
 * @see [[org.apache.kafka.common.config.TopicConfig]]
 */
case class Topic(
    name: TopicName,
    partitions: Int = 3,
    replicationFactor: Int = 1,
    cleanupPolicy: CleanupPolicy = Compact(),
    retention: Option[Retention] = Some(InfiniteRetention)
) extends KafkaPropType {

  override def toPropMap: Map[String, String] =
    cleanupPolicy.toPropMap ++ retention.map(_.toPropMap).getOrElse(Map.empty)

  override def configEntries: Seq[ConfigEntry] =
    retention
      .map(_.configEntries)
      .getOrElse(Seq.empty) ++ cleanupPolicy.configEntries

  def newTopic: NewTopic =
    new NewTopic(name, partitions, replicationFactor.toShort)
      .configs(toPropMap.asJava)

}

object Topic extends ExtraConfigReaders {

  def fromConfigFile(f: java.io.File): Seq[Topic] = {
    val cfg = ConfigFactory.parseFile(f)
    cfg
      .getConfigList("net.scalytica.kafka.admin.topics")
      .asScala
      .map(_.as[Topic])
  }

}
