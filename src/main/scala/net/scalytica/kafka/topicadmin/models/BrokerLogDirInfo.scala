package net.scalytica.kafka.topicadmin.models

import java.util.{Map => JMap}

import org.apache.kafka.common.requests.DescribeLogDirsResponse.{
  LogDirInfo,
  ReplicaInfo
}
import org.apache.kafka.common.{TopicPartition => KTopicPartition}

import scala.collection.JavaConverters._

case class BrokerLogDirInfo(
    id: Int,
    directories: Seq[LogDirectoryInfo]
) {

  override def toString: String = {
    s"""Log directory information for broker: #$id:
       |${directories.mkString("\n")}
     """.stripMargin
  }

}

object BrokerLogDirInfo {

  implicit def fromKafkaModel(
      details: (java.lang.Integer, JMap[String, LogDirInfo])
  ): BrokerLogDirInfo = {
    BrokerLogDirInfo(
      id = details._1.intValue(),
      directories = details._2
    )
  }

}

case class LogDirectoryInfo(
    directory: String,
    // TODO: Add property to keep error code.
    info: Map[TopicPartition, ReplicaInformation]
) {

  private[this] def infoAsString(
      i: (TopicPartition, ReplicaInformation)
  ): String = {
    s"""  - Topic: ${i._1.topic}
       |    - partition : ${i._1.partition}
       |    - size      : ${i._2.size}
       |    - offset lag: ${i._2.offsetLag}
       |    - is future : ${i._2.isFuture}
     """.stripMargin
  }

  override def toString: String = {
    s"""Log directory info: $directory
       |${info.map(infoAsString).mkString("\n")}
     """.stripMargin
  }

}

object LogDirectoryInfo {

  implicit def infoFromKafkaModelMap(
      i: (String, JMap[KTopicPartition, ReplicaInfo])
  ): LogDirectoryInfo = {
    LogDirectoryInfo(
      directory = i._1,
      info = i._2.asScala.toMap.map {
        case (tp, ri) => (tp: TopicPartition, ri: ReplicaInformation)
      }
    )
  }

  implicit def infoMapFromKafkaModel(
      m: JMap[String, LogDirInfo]
  ): Seq[LogDirectoryInfo] = {
    m.asScala.map(kv => kv._1 -> kv._2.replicaInfos: LogDirectoryInfo).toSeq
  }

}
