package net.scalytica.kafka.topicadmin.cli

import Commands._
import net.scalytica.kafka.topicadmin.client.KafkaAdminClient

import scala.concurrent.{ExecutionContext, Future}

case class CliOptions(
    brokers: String = "localhost:29092",
    user: Option[String] = None,
    pass: Option[String] = None,
    command: Option[Cmd] = None
) {

  def enrich[T <: Cmd](f: PartialFunction[Cmd, T]): CliOptions =
    copy(command = command.collect(f))

  def execute()(
      implicit adminClient: KafkaAdminClient,
      ec: ExecutionContext
  ): Future[String] = {
    command.map(_.execute()).getOrElse {
      Future.failed(new IllegalArgumentException("No command provided"))
    }
  }

}
