package net.scalytica.kafka.topicadmin.cli

import java.io.File

import net.scalytica.kafka.topicadmin.client.KafkaAdminClient
import net.scalytica.kafka.topicadmin.models._
import net.scalytica.kafka.topicadmin.{BrokerId, TopicName}

import scala.concurrent.{ExecutionContext, Future}

object Commands {

  sealed trait Cmd {

    def execute()(
        implicit client: KafkaAdminClient,
        ec: ExecutionContext
    ): Future[String]

  }

  private[this] def unhandledWithMessage(msg: String): Future[String] = {
    Future.failed[String] {
      new IllegalArgumentException(msg)
    }
  }

  private[this] def unhandledCommand[T <: Cmd](cmd: T): Future[String] = {
    unhandledWithMessage("Unhandled command:\n   - " + cmd)
  }

  case class ShowTopicNames(
      includeInternal: Boolean = false
  ) extends Cmd {

    def withIncludeInternalEnabled: ShowTopicNames = copy(true)

    override def execute()(
        implicit client: KafkaAdminClient,
        ec: ExecutionContext
    ): Future[String] = {
      val res =
        if (!includeInternal) client.listTopicNames()
        else client.listAllTopicNames()

      res.map { res =>
        if (res.isEmpty) "Could not find any topics"
        else s"Found topics:${res.mkString("\n", "\n", "")}"
      }
    }
  }

  object ShowTopicNames { val name = "topic-names" }

  /**
   * Command for displaying topic information
   */
  case class DescribeTopic(
      topics: Seq[TopicName] = Seq.empty,
      showConfig: Boolean = false
  ) extends Cmd {

    def withTopic(t: TopicName): DescribeTopic = copy(topics = topics :+ t)
    def withTopics(t: Seq[TopicName]): DescribeTopic =
      copy(topics = topics ++ t)
    def withShowConfigEnabled: DescribeTopic = copy(showConfig = true)

    override def execute()(
        implicit client: KafkaAdminClient,
        ec: ExecutionContext
    ): Future[String] = {
      if (topics.nonEmpty) {
        client.describeTopicConfig(topics: _*).map(_.mkString("\n", "\n", ""))
      } else {
        unhandledCommand(this)
      }
    }
  }

  object DescribeTopic { val name = "describe-topic" }

  /**
   * Command for describing topics or broker log directories
   */
  case class DescribeLogDirs(
      topic: Option[TopicName] = None,
      brokers: Seq[BrokerId] = Seq.empty
  ) extends Cmd {

    def withTopic(t: TopicName): DescribeLogDirs = copy(topic = Some(t))
    def withBrokers(b: BrokerId*): DescribeLogDirs =
      copy(brokers = brokers ++ b)

    override def execute()(
        implicit client: KafkaAdminClient,
        ec: ExecutionContext
    ): Future[String] = {
      if (topic.isEmpty && brokers.isEmpty) unhandledCommand(this)
      else {
        topic.flatMap { t =>
          if (brokers.nonEmpty) {
            Some(
              client
                .describeLogDirectoriesForTopic(brokers, t)
                .map(_.mkString("\n", "\n", ""))
            )
          } else {
            None
          }
        }.getOrElse {
          client.describeLogDirectories(brokers: _*).map { res =>
            res.mkString("\n", "\n", "")
          }
        }
      }
    }
  }

  object DescribeLogDirs { val name = "describe-log-dirs" }

  /**
   * Command for checking if a topic exists
   */
  case class TopicExists(topics: Seq[TopicName] = Seq.empty) extends Cmd {

    def withTopics(t: Seq[TopicName]): TopicExists = copy(topics = topics ++ t)

    override def execute()(
        implicit client: KafkaAdminClient,
        ec: ExecutionContext
    ): Future[String] = unhandledCommand(this)
  }

  /**
   * Command for creating a topic.
   */
  case class CreateTopic(
      name: Option[TopicName] = None,
      partitions: Int = 3,
      replFactor: Int = 1,
      cleanupPolicy: CleanupPolicy = Compact(),
      retention: Retention = InfiniteRetention
  ) extends Cmd {

    def withName(t: TopicName): CreateTopic        = copy(name = Some(t))
    def withPartitions(p: Int): CreateTopic        = copy(partitions = p)
    def withReplicationFactor(r: Int): CreateTopic = copy(replFactor = r)
    def withRetention(r: Retention): CreateTopic   = copy(retention = r)
    def withCleanupPolicy(c: CleanupPolicy): CreateTopic =
      copy(cleanupPolicy = c)

    override def execute()(
        implicit client: KafkaAdminClient,
        ec: ExecutionContext
    ): Future[String] = {
      name.map { n =>
        val t = Topic(
          name = n,
          partitions = partitions,
          replicationFactor = replFactor,
          cleanupPolicy = cleanupPolicy,
          retention = Some(retention)
        )
        client.createTopics(t).map {
          case created: Boolean if created => s"Topic $n successfully created."
          case _                           => s"Error when creating topic $n"
        }
      }.getOrElse {
        unhandledWithMessage("Topic name is a required parameter.")
      }
    }

    def topicOption: Option[Topic] = {
      name.map { n =>
        Topic(
          name = n,
          partitions = partitions,
          replicationFactor = replFactor,
          cleanupPolicy = cleanupPolicy,
          retention = Some(retention)
        )
      }
    }
  }

  object CreateTopic { val name = "create-topic" }

  /**
   * Command for creating / updating a batch of topics.
   */
  case class CreateOrUpdateTopics(
      commands: Seq[CreateTopic] = Seq.empty
  ) extends Cmd {

    def withCommands(cmd: CreateTopic*): CreateOrUpdateTopics =
      copy(commands = cmd)

    override def execute()(
        implicit client: KafkaAdminClient,
        ec: ExecutionContext
    ): Future[String] = {
      client.createOrUpdateTopics(commands.flatMap(_.topicOption): _*).map {
        case applied: Boolean if applied =>
          "Topics successfully created / updated."

        case _ =>
          "Error when creating / updating topics"
      }
    }

  }

  object CreateOrUpdateTopics {

    val name = "create-update-topics"

    def fromFile(f: File): CreateOrUpdateTopics = {
      val topics = Topic.fromConfigFile(f).map { t =>
        CreateTopic(
          name = Some(t.name),
          partitions = t.partitions,
          replFactor = t.replicationFactor,
          cleanupPolicy = t.cleanupPolicy,
          retention = t.retention.getOrElse(InfiniteRetention)
        )
      }

      CreateOrUpdateTopics(topics)
    }

    def parse(s: Seq[String])(parser: CliParser): CreateOrUpdateTopics = {
      def lineParser(s: String): Map[String, String] = {
        s.split(";")
          .map { prop =>
            prop.split("=").toList match {
              case key :: value :: Nil => Some(key -> value)
              case _                   => None // ignore because invalid
            }
          }
          .collect { case Some(kv) => kv._1 -> kv._2 }
          .toMap
      }

      val cfgLines = s.map(_.stripPrefix("{").stripSuffix("}"))
      val cfgMaps  = cfgLines.map(lineParser)

      val cmds = cfgMaps.map { m =>
        val args = m.foldLeft(List(CreateTopic.name)) { (commands, curr) =>
          val c = {
            if (curr._1 == "name") List(curr._2)
            else List(s"--${curr._1}", curr._2)
          }
          commands ++ c
        }
        parser.parse(args, CliOptions()).flatMap {
          _.command.flatMap {
            case ct: CreateTopic => Some(ct)
            case _               => None
          }
        }
      }.collect { case Some(cmd) => cmd }

      CreateOrUpdateTopics(cmds)
    }

  }

  /**
   * Command for updating a topic
   *
   * TODO: Complete me
   */
  case class UpdateTopicPartitions(
      topic: Option[TopicName] = None,
      numPartitions: Option[Int] = None
  ) extends Cmd {

    def withTopic(t: TopicName): UpdateTopicPartitions = copy(topic = Some(t))
    def withPartitions(p: Int): UpdateTopicPartitions =
      copy(numPartitions = Some(p))

    override def execute()(
        implicit client: KafkaAdminClient,
        ec: ExecutionContext
    ): Future[String] = unhandledCommand(this)
  }

  object UpdateTopicPartitions { val name = "update-topic" }

  /**
   * Command for deleting a topic
   */
  case class DeleteTopic(topic: TopicName = "") extends Cmd {

    def withTopic(t: TopicName): DeleteTopic = copy(topic = t)

    override def execute()(
        implicit client: KafkaAdminClient,
        ec: ExecutionContext
    ): Future[String] = unhandledCommand(this)
  }

  object DeleteTopic { val name = "delete-topic" }

  /**
   * NoOp command...does nothing
   */
  case object NoOp extends Cmd {
    val name = ""

    override def execute()(
        implicit client: KafkaAdminClient,
        ec: ExecutionContext
    ): Future[String] = unhandledWithMessage("No command provided")
  }

}
