package net.scalytica.kafka

import org.apache.kafka.common.KafkaFuture

import scala.concurrent.{Future, Promise}
import scala.util.Try
import scala.util.control.NonFatal
import scala.collection.JavaConverters._

package object topicadmin {

  type TopicName = String
  type BrokerId  = Int

  implicit def scalaIntToJavaInteger(i: Int): java.lang.Integer =
    Integer.valueOf(i)

  implicit def scalaIntSeqToJavaIntegerCollection(
      sis: Seq[Int]
  ): java.util.Collection[java.lang.Integer] = {
    sis.map(scalaIntToJavaInteger).asJava
  }

  implicit class StringExt(str: String) {
    def safeString: Option[String] = Option(str).filterNot(_.isEmpty)
  }

  object KafkaFutureImplicits {

    implicit class KafkaFutureConverter[A](val k: KafkaFuture[A])
        extends AnyVal {

      def call[B](f: A => B): Future[B] = {
        val promise = Promise[B]()

        try {
          k.thenApply { n: A =>
            promise.complete(Try(f(n)))
          }
        } catch {
          case NonFatal(e) => promise.failure(e)
        }

        promise.future
      }

    }

    implicit class KafkaFutureVoidConverter(val k: KafkaFuture[Void])
        extends AnyVal {

      def callVoid(): Future[Unit] = k.call(_ => ())

    }
  }

}
